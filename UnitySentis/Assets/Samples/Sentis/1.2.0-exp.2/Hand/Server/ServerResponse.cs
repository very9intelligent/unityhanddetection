using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ai.server
{

    public class ServerResponse
    {
        public int right_hand_detected = 0;
        public int left_hand_detected = 0;
        public float left_hand_x = 0;
        public float left_hand_y = 0;
        public ai.hand.Hand.HandPose left_hand_pose = hand.Hand.HandPose.OPEN_HAND;
        public float right_hand_x = 0;
        public float right_hand_y = 0;
        public ai.hand.Hand.HandPose right_hand_pose = hand.Hand.HandPose.OPEN_HAND;
    }

}