using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace ai.ui
{

    public class ShowHandLabel : MonoBehaviour
    {

        public TMPro.TMP_Text leftLabel;
        public TMPro.TMP_Text rightLabel;
        private ai.server.UnityHandServer server;
        public RawImage previewUI;

        // Start is called before the first frame update
        void Start()
        {
            leftLabel.gameObject.SetActive(false);
            rightLabel.gameObject.SetActive(false);
            server = GetComponent<ai.server.UnityHandServer>();
        }

        // Update is called once per frame
        void Update()
        {
            if (server.lastResponse.left_hand_detected == 1)
            {
                leftLabel.gameObject.SetActive(true);
                Vector3[] corners = new Vector3[4];
                previewUI.rectTransform.GetWorldCorners(corners);
                Vector3 delta = (corners[2] - corners[0]);
                delta = new Vector3(corners[0].x + delta.x * (server.lastResponse.left_hand_x),
                    corners[0].y + delta.y * (server.lastResponse.left_hand_y), delta.z);
                
                leftLabel.gameObject.transform.position = delta;
                leftLabel.text = "LEFT_HAND\n" + server.lastResponse.left_hand_pose;
            }
            else
            {
                leftLabel.gameObject.SetActive(false);
            }
            if (server.lastResponse.right_hand_detected == 1)
            {
                rightLabel.gameObject.SetActive(true);
                Vector3[] corners = new Vector3[4];
                previewUI.rectTransform.GetWorldCorners(corners);
                Vector3 delta = (corners[2] - corners[0]);
                delta = new Vector3(corners[0].x + delta.x * (server.lastResponse.right_hand_x),
                    corners[0].y + delta.y * (server.lastResponse.right_hand_y), delta.z);
                rightLabel.gameObject.transform.position = delta;
                rightLabel.text = "RIGHT_HAND\n" + server.lastResponse.right_hand_pose;
            }
            else
            {
                rightLabel.gameObject.SetActive(false);
            }
        }
    }

}