using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ai.hand
{
    [RequireComponent(typeof(HandTracking))]
    public class HandPoseEstimation : MonoBehaviour
    {
        public int maxHist = 3;
        public List<ai.hand.Hand> leftHandHist = new List<ai.hand.Hand>();
        public List<ai.hand.Hand> righttHandHist = new List<ai.hand.Hand>();

        static readonly Dictionary<string, int> dictLandmarkName =
            new Dictionary<string, int>()
            {
                {"WRIST" , 0},
                {"THUMB_CMC" , 1},
                {"THUMB_MCP" , 2},
                {"THUMB_IP" , 3},
                {"THUMB_TIP" , 4},
                {"INDEX_FINGER_MCP" , 5},
                {"INDEX_FINGER_PIP" , 6},
                {"INDEX_FINGER_DIP" , 7},
                {"INDEX_FINGER_TIP" , 8},
                {"MIDDLE_FINGER_MCP" , 9},
                {"MIDDLE_FINGER_PIP" , 10},
                {"MIDDLE_FINGER_DIP" , 11},
                {"MIDDLE_FINGER_TIP" , 12},
                {"RING_FINGER_MCP" , 13},
                {"RING_FINGER_PIP" , 14},
                {"RING_FINGER_DIP" , 15},
                {"RING_FINGER_TIP" , 16},
                {"PINKY_MCP" , 17},
                {"PINKY_PIP" , 18},
                {"PINKY_DIP" , 19},
                {"PINKY_TIP" , 20},

            };

        public void RunInference(HandCollection handCollection, HandTracking handTracking)
        {
            // Get current left hand
            RunOneHand(leftHandHist, handTracking.currentMainLeftHand);
            // Get current right hand
            RunOneHand(righttHandHist, handTracking.currentMainRightHand);
            //Debug.Log("Nb left : " + leftHandHist.Count + " , Nb right : " + righttHandHist.Count);
            if (handTracking.currentMainLeftHand != null)
                handTracking.currentMainLeftHand.CountingFingersToPose(CountFingers(leftHandHist, new List<string> { "INDEX_FINGER", "MIDDLE_FINGER",
                "RING_FINGER","PINKY" }));
            if (handTracking.currentMainRightHand != null)
                handTracking.currentMainRightHand.CountingFingersToPose(CountFingers(righttHandHist, new List<string> { "INDEX_FINGER", "MIDDLE_FINGER",
                "RING_FINGER","PINKY" }));

            /*if (handTracking.currentMainRightHand != null && handTracking.currentMainLeftHand != null)
                Debug.Log("Nb left open : " + handTracking.currentMainRightHand.handPose.ToString()
                    + ", Nb right open : " + handTracking.currentMainLeftHand.handPose.ToString()); */
        }

        public void RunOneHand(List<ai.hand.Hand> handList, ai.hand.Hand currentHand)
        {
            // handCollection.idHandDict[keyofmax].RemoveRange(0, Mathf.Min(itemsToRemove, handCollection.idHandDict[keyofmax].Count));
            // If no hand is found, remove the first frame from list
            if (currentHand == null)
            {
                handList.RemoveRange(0, Mathf.Min(1, handList.Count));
            }
            // If hand is found
            else
            {
                int itemsToRemove = (handList.Count + 1) - maxHist;
                if(itemsToRemove > 0)
                    handList.RemoveRange(0, Mathf.Min(itemsToRemove, handList.Count));
                handList.Add(currentHand);
            }
        }

        public int CountFingers(List<ai.hand.Hand> handHist, List<string> listFinger)
        {
            int count = 0;
            foreach (string finger in listFinger)
                if (CheckFingerOpenHist(finger, handHist))
                    count += 1;
            return count;
        }

        public bool CheckFingerOpenHist(string fingerName,List<ai.hand.Hand> handHist)
        {
            // First check if we have enough hands to check
            if (handHist.Count == 0)
                return false;
            // Otherwise if one is false then all is false
            foreach (ai.hand.Hand hand in handHist)
                if (FingerIsOpen(fingerName, hand) == false)
                    return false;
            return true;
        }

        public bool FingerIsOpen(string fingerName, ai.hand.Hand hand)
        {
            // Get center of wrist, finger tip and center
            Vector3Int wrist = hand.handNodes[dictLandmarkName["WRIST"]];
            Vector3Int tip = hand.handNodes[dictLandmarkName[fingerName+"_TIP"]];
            Vector3Int pip = hand.handNodes[dictLandmarkName[fingerName+"_PIP"]];
            // Distances
            float dist_tip = Vector2Int.Distance(
                new Vector2Int(wrist.x, wrist.y),
                new Vector2Int(tip.x, tip.y)
                );
            float dist_pip = Vector2Int.Distance(
                new Vector2Int(wrist.x, wrist.y),
                new Vector2Int(pip.x, pip.y)
                );
            return (dist_tip >= dist_pip);
        }

        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {

        }

        
    }

}