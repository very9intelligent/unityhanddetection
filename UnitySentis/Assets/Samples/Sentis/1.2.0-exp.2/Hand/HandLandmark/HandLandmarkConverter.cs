using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Sentis;
using Unity.Sentis.ONNX;


public class HandLandmarkConverter : MonoBehaviour
{
    // Start is called before the first frame update
    public ModelAsset modelAsset;
    Model runtimeModel;
    public ONNXModelConverter converter;




    void Start()
    {
        //runtimeModel = ModelLoader.Load(Application.streamingAssetsPath + "/hand_landmark_sparse_Nx3x224x224.sentis");

        /*runtimeModel = ModelLoader.Load(modelAsset);*/
        converter = new ONNXModelConverter(true);
        Debug.Log("Starting model convertion...");
        var md = converter.Convert(Application.streamingAssetsPath + "/hand.onnx",
            Application.streamingAssetsPath + "/");
        ModelWriter.Save(Application.streamingAssetsPath + "/hand.sentis", md);
        Debug.Log(md);
        Debug.Log("Model saved...");
    }
}
