using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandUtils
{
    public static Texture2D toTexture2D(RenderTexture rTex, int xmin, int xmax, int ymin, int ymax)
    {
        // Define the parameters for the ReadPixels operation
        Rect regionToReadFrom = new Rect(0, 0, Screen.width, Screen.height);

        Texture2D tex = new Texture2D(xmax - xmin + 1, ymax - ymin + 1, TextureFormat.RGB24, false);
        // ReadPixels looks at the active RenderTexture.
        RenderTexture.active = rTex;
        tex.ReadPixels(new Rect(xmin, ymin, xmax, ymax), 0, 0);
        tex.Apply();
        return tex;
    }

    static public void DrawLine(Texture2D a_Texture, int x1, int y1, int x2, int y2, Color a_Color)
    {
        float xPix = x1;
        float yPix = y1;

        float width = x2 - x1;
        float height = y2 - y1;
        float length = Mathf.Abs(width);
        if (Mathf.Abs(height) > length) length = Mathf.Abs(height);
        int intLength = (int)length;
        float dx = width / (float)length;
        float dy = height / (float)length;
        for (int i = 0; i <= intLength; i++)
        {
            a_Texture.SetPixel((int)xPix, (int)yPix, a_Color);

            xPix += dx;
            yPix += dy;
        }
    }


}
