﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ExampleCSV : MonoBehaviour {

	void Awake() {
	
		List<Dictionary<string,object>> data = CSVReader.Read ("anchors_2944");
		
		for(var i=0; i < data.Count; i++) {
			print ("name " + data[i]["x"] + " " +
				   "age " + data[i]["y"] + " " +
			       "speed " + data[i]["w"] + " " +
			       "desc " + data[i]["h"]);
		}
	
	}

	// Use this for initialization
	void Start () {	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
