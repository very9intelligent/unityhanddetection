using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ai.hand {
    
    public class HandCollection : MonoBehaviour
    {
        
        public Dictionary<int, List<Hand>> idHandDict;
        public List<Hand> handsOnScreen;
        public int maxHist = 3;
        public float overlatpingArea;
        public int maxId = 100;

        // Start is called before the first frame update
        void Start()
        {
            idHandDict = new Dictionary<int, List<Hand>>();
            handsOnScreen = new List<Hand>();
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }

}