using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Sentis;
using Unity.Sentis.ONNX;

public class PalmDetectionConverter : MonoBehaviour
{
    // Start is called before the first frame update
    public ModelAsset modelAsset;
    Model runtimeModel;
    public ONNXModelConverter converter;




    void Start()
    {
        //runtimeModel = ModelLoader.Load(Application.streamingAssetsPath + "/palm_detection_full_inf_post_192x192.sentis");
        
        //runtimeModel = ModelLoader.Load(modelAsset);
        converter = new ONNXModelConverter(true);
        Debug.Log("Starting model...");
        var md = converter.Convert(Application.streamingAssetsPath + "/palm_detection_full_inf_post_192x192.onnx",
            Application.streamingAssetsPath + "/");
        ModelWriter.Save(Application.streamingAssetsPath + "/palm_detection_full_inf_post_192x192.sentis", md);
        Debug.Log(md);
    }
}
