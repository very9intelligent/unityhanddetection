﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

namespace utils.level
{

    public class LevelTransitionController : MonoBehaviour
    {

        private Animator anim;
        private string levelToLoad;
        public bool ChangeLevel;
        private AsyncOperation loadingOperation;
        public Slider progressBar;
        public bool inTransition;


        // Start is called before the first frame update
        void Start()
        {
            anim = transform.GetComponent<Animator>();
        }

        // Update is called once per frame
        void Update()
        {
        }

        public void ResetController()
        {
            inTransition = false;
        }

        public void FullTransitionTo(string levelToLoad, bool ChangeLevel, string transitiontype = "FadeIn_Transparency")
        {
            this.ChangeLevel = ChangeLevel;
            this.levelToLoad = levelToLoad;
            anim.Play(transitiontype);
            inTransition = true;
        }

        public void AssyncLoad()
        {
            if (ChangeLevel && levelToLoad != null)
            {
                loadingOperation = SceneManager.LoadSceneAsync(levelToLoad);
                StartCoroutine(FadeLoadingSceneScreen());
            }
            else
            {
                PlayFadeOut();
            }
        }

        public void SyncLoad(string levelToLoad)
        {
            SceneManager.LoadSceneAsync(levelToLoad);
        }

        public void PlayFadeOut()
        {
            anim.Play("FadeOut_Transparency");
        }

        IEnumerator FadeLoadingSceneScreen()
        {
            progressBar.value = 0;
            while (progressBar.value < 0.9f) {
                progressBar.value = Mathf.Clamp01(loadingOperation.progress / 0.9f);
                yield return null;
            }
            progressBar.value = 1f;
            PlayFadeOut();
        }

        public void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

    }

}