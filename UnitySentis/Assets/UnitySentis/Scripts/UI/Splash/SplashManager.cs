using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ui.splash { 

    /// <summary>
    /// Code used to make transition between splash elements
    /// </summary>
    public class SplashManager : MonoBehaviour
    {

        public float time = 5f;
        public AnimationCurve animationCurve;

        public List<CanvasGroup> transitionElements;


        private float currenttime=0f;
        private int groupindex = 0;

        public UnityEvent animationEndEvent;

        // Start is called before the first frame update
        void Start()
        {
            currenttime = 0f;
            groupindex = 0;
        }

        // Update is called once per frame
        void Update()
        {
            // If we didn't go through all splash elements
            if(groupindex < transitionElements.Count)
            {
                // If we finished the current splash element
                if(currenttime > time)
                {
                    groupindex++;
                    // Trigger Action
                    if(groupindex >= transitionElements.Count)
                    {
                        animationEndEvent?.Invoke();
                    }
                } 
                else
                {
                    transitionElements[groupindex].alpha = Mathf.Min(Mathf.Max(animationCurve.Evaluate(currenttime / time), 0), 1f);
                    currenttime += Time.deltaTime;
                    Debug.Log(currenttime);
                }
            }
        }



    }

}